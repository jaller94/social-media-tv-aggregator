import FeedParser from 'feedparser';
import StatusList from './status-list.js';

const statusList = new StatusList();

const statuses = [];

function getStatusFeed(callback) {
    const reqListener = function () {
        if(this.status >= 400) 
            callback(new Error(`${url} replied ${xhr.status}`));
        callback(null, this.responseText);
    }

    const oReq = new XMLHttpRequest();
    oReq.addEventListener('loadend', reqListener);
    oReq.addEventListener('abort', (event) => {callback(event.error)});
    oReq.addEventListener('error', (event) => {callback(event.error)});
    oReq.open('GET', 'feed.atom');
    oReq.send();
}

function actionReloadFeed() {
    const feedParser = new FeedParser();

    feedParser.on('readable', function () {
        const stream = this; // `this` is `feedParser`, which is a stream
        let status;

        while (status = stream.read()) {
            statuses.push(status);
            if (statuses.length === 1) slideshow(0);
        }
    });

    console.log('Action: Refresh feed');
    getStatusFeed((err, data) => {
        if (err) {
            console.error(err);
        }
        statuses.length = 0;
        feedParser.write(data);
    });
}

function actionReloadPage() {
    // Reload the current page, without using the cache
    window.location.reload(true);
}

setInterval(actionReloadFeed, 1 * 60 * 60 * 1000); // Reload feed every hour
actionReloadFeed();

setTimeout(actionReloadPage, 24 * 60 * 60 * 1000); // Full reload every 24 hours

let timeout;

function slideshow(number) {
    if (number >= statuses.length) {
        number = 0;
    }
    showArticle(statuses[number]);
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(() => slideshow(number+1), 10000);
}

function showArticle(status) {
    const image = document.getElementsByClassName('post-image')[0];
    const description = document.getElementsByClassName('post-description')[0];
    const date = document.getElementsByClassName('post-date')[0];
    const authorName = document.getElementsByClassName('post-author-name')[0];
    const authorImage = document.getElementsByClassName('post-author-image')[0];
    //TODO Hard-coded values
    authorName.innerText = '@7geesehq';
    authorImage.src = 'https://instagram.fyvr4-1.fna.fbcdn.net/vp/98ebe722fc0b52f4e2caae8f3e28bb7e/5B8603FF/t51.2885-19/11248374_1564052150526293_1905267535_a.jpg';
    if (status.image && status.image.url) {
        image.src = status.image.url;
    }
    description.innerText = '';
    description.append(generateDescription(status.description));
    const options = {
        weekday: 'short', year: 'numeric', month: 'short',
        day: 'numeric', hour: '2-digit', minute: '2-digit',
    };
    const dateObj = new Date(status.date);
    const dateString = dateObj.toLocaleTimeString('en-CA', options);
    date.innerText = dateString || 'No date';
}

function generateDescription(text) {
    const root = document.createElement('div');
    text = text.replace(/<a.*\/a>/, '');

    for (const paragraph of text.split('\n')) {
        const element = document.createElement('p');
        element.innerText = paragraph;
        root.append(element);
    }

    return root;
}
