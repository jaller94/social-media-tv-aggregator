export default class StatusList {
    constructor() {
        this.currentIndex = 0;
        this.list = [];
    }

    add(status) {
        this.list.push(status);
    }

    getCurrent() {
        const current = this.list[currentIndex];
        return current ? current : null;
    }

    getLength() {
        return;
    }

    getNext() {
        this.currentIndex++;
        if (this.currentIndex >= this.list.length) {
            this.currentIndex = 0;
        }
        return getCurrent();
    }
}
