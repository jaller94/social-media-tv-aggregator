# Social Media TV Aggregator
Displays public content of your social media channels. Automatically rotates through the posts, making it suitable for TV use.

## Setup
```
npm ci
npm run-script build
```

Now copy the files from the folder `dist` to a webspace you wish to host the slideshow on.

## Development
The used term for a social media publication is status.
Statuses are expected to have images.

```
npm install
npm run-script start:dev
```
